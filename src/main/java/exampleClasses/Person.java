package exampleClasses;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Person {
	private String firstName;
	private String lastName;
	private int age;
	private String gender;
	
	private Address address;
	
	public Person(String fName, String lName, int a, String g, Address address) {
		firstName = fName;
		lastName = lName;
		age = a;
		gender = g;
		this.address = address;
	}
	/*
	
	}*/
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("Person.xml");
		Person person = (Person).getBean("Person1");
	}

}
